let trainer = {
	name: 'Ash Ketchum',
	age:10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
		talk: function(){
		}
	};

	console.log(trainer);
	console.log('Result of dot notation: ');
	trainer.walk = function(){
		console.log(this.name);
	};

	trainer.walk();
	
	console.log('Result of square bracket notation: ');
	trainer.walk = function(){
		console.log(this.pokemon);
	};

	trainer.walk();

	console.log('Result of talk method')

	trainer.talk();
	console.log('Pikachu! I choose you!');

	class Pokemon {
		constructor(name, level, health, attack, tackle){
			this.name = name;
			this.level = level;
			this.health = health;
			this.attack = attack;

			this.tackle = function (target) {
				
				console.log(this.name + ' tackled ' + target.name);
				console.log(target.name + "'s health is now reduced to " + Number(target.health = target.health - this.attack));
				if(target.health <= 0){
					console.log(target.name + ' fainted');
				} 
			}
		}
	}

	let pokemonA = new Pokemon('Pikachu', 12, 24, 12);	
	let pokemonB = new Pokemon('Geodude', 8, 16, 8);	
	let pokemonC = new Pokemon('Mewtwo', 100, 200, 100);	

	console.log(pokemonA);
	console.log(pokemonB);
	console.log(pokemonC);

	pokemonB.tackle(pokemonA);
	console.log(pokemonA);

	pokemonC.tackle(pokemonB);
	console.log(pokemonB);
